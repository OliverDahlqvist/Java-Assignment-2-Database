CREATE TABLE IF NOT EXISTS superhero_power (
    superhero_id int,
    power_id int,
    PRIMARY KEY(superhero_id, power_id)
);

ALTER TABLE superhero_power 
ADD CONSTRAINT superheropower_fk_superhero FOREIGN KEY (superhero_id) REFERENCES superhero,
ADD CONSTRAINT superheropower_fk_power FOREIGN KEY (power_id) REFERENCES "power";

SELECT * FROM superhero_power;