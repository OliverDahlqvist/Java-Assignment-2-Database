CREATE TABLE IF NOT EXISTS superhero (
    superhero_id SERIAL PRIMARY KEY,
    superhero_name VARCHAR,
    superhero_alias VARCHAR,
    superhero_origin VARCHAR
);

SELECT * FROM superhero;

CREATE TABLE IF NOT EXISTS assistant (
    assistant_id SERIAL PRIMARY KEY,
    assistant_name VARCHAR
);

SELECT * FROM assistant;

CREATE TABLE IF NOT EXISTS "power" (
    power_id SERIAL PRIMARY KEY,
    power_name VARCHAR,
    power_description VARCHAR
);