INSERT INTO superhero (superhero_name, superhero_alias, superhero_origin)
VALUES ('Nemo', 'The Fish', 'Great Barrier Reef');
INSERT INTO superhero (superhero_name, superhero_alias, superhero_origin)
VALUES ('Doris', 'The Blue Fish', 'Great Barrier Reef');
INSERT INTO superhero (superhero_name, superhero_alias, superhero_origin)
VALUES ('Marvin', 'The Real Hero', 'Great Barrier Reef');

SELECT * FROM superhero