INSERT INTO "power" (power_name, power_description)
VALUES ('Bubble blow', 'Blows bubbles');
INSERT INTO "power" (power_name, power_description)
VALUES ('Under water breathing', 'Able to breath under water');
INSERT INTO "power" (power_name, power_description)
VALUES ('Faster swimmer', 'Able to swim extremely fast');
INSERT INTO "power" (power_name, power_description)
VALUES ('Night vision', 'Able to see in the dark');

INSERT INTO superhero_power (superhero_id, power_id)
VALUES (1, 1);
INSERT INTO superhero_power (superhero_id, power_id)
VALUES (1, 2);
INSERT INTO superhero_power (superhero_id, power_id)
VALUES (1, 3);
INSERT INTO superhero_power (superhero_id, power_id)
VALUES (1, 4);
INSERT INTO superhero_power (superhero_id, power_id)
VALUES (2, 3);
INSERT INTO superhero_power (superhero_id, power_id)
VALUES (3, 1);

SELECT * FROM superhero_power