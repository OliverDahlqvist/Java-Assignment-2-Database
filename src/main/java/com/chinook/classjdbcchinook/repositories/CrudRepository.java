package com.chinook.classjdbcchinook.repositories;

import java.util.List;

/**
 * Base CrudRepository interface which contains all basic CRUD-operations.
 * @param <T> Object type which the repository is supposed to handle.
 * @param <ID> The key identifier type which should be the same type as the primary key of the specified type.
 */
public interface CrudRepository <T, ID> {

    /**
     * Finds all entities of certain type in the connected database.
     * @return List of specified entity type.
     */
    List<T> findAll();

    /**
     * Finds specified object by their primary key from the connected database.
     * @param id Primary key to query for.
     * @return Found object, else null.
     */
    T findById(ID id);

    /**
     * Inserts a new object into the connected database.
     * @param object Object which should be inserted.
     * @return Rows affected by query.
     */
    int insert(T object);

    /**
     * Updates the specified object in the connected database.
     * Uses the object's id property as identifier for which object to update.
     * @param object Object to update.
     * @return Rows affected by query.
     */
    int update(T object);
}
