package com.chinook.classjdbcchinook.repositories.customer;
import com.chinook.classjdbcchinook.models.Customer;
import com.chinook.classjdbcchinook.models.CustomerCountry;
import com.chinook.classjdbcchinook.models.CustomerGenre;
import com.chinook.classjdbcchinook.models.CustomerSpender;
import com.chinook.classjdbcchinook.repositories.CrudRepository;

import java.util.List;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {

    /**
     * Finds customer by input string.
     * @param name Name used to search.
     * @return Found customer with given name.
     */
    Customer findByName(String name);

    /**
     * Finds a list of customers within the given limit and with an offset.
     * @param limit Amount of customers to query.
     * @param offset Offset index to start returning customers from.
     * @return List of customers within given limit and offset.
     */
    List<Customer> findWithLimitAndOffset(int limit, int offset);

    /**
     * Finds the country with most customers.
     * @return Country with most customers.
     */
    CustomerCountry findCountryWithMostCustomers();

    /**
     * Finds the customer with the highest spending sum.
     * @return Returns the customer with the highest spending based on their invoices.
     */
    CustomerSpender findHighestSpender();

    /**
     * Finds the customers most popular genre.
     * @return Returns the name of the genre that is most popular.
     */
    List<CustomerGenre> findMostPopularGenre(Customer customer);
}
