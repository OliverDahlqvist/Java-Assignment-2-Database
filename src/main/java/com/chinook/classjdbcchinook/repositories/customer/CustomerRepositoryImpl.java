package com.chinook.classjdbcchinook.repositories.customer;

import com.chinook.classjdbcchinook.models.Customer;
import com.chinook.classjdbcchinook.models.CustomerCountry;
import com.chinook.classjdbcchinook.models.CustomerGenre;
import com.chinook.classjdbcchinook.models.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {
    private String url;
    private String username;
    private String password;

    public CustomerRepositoryImpl(@Value("${spring.datasource.url}") String url,
                                  @Value("${spring.datasource.username}") String username,
                                  @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public List<Customer> findAll() {
        List<Customer> customers = new ArrayList<>();
        String sql = "SELECT * FROM customer";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")));
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    @Override
    public Customer findById(Integer id) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE customer_id = ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email"));
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    @Override
    public int insert(Customer customer) {
        int rowsAffected = 0;
        String sql = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) VALUES (?, ?, ?, ?, ?, ?)";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, customer.firstName());
            preparedStatement.setString(2, customer.lastName());
            preparedStatement.setString(3, customer.country());
            preparedStatement.setString(4, customer.postalCode());
            preparedStatement.setString(5, customer.phoneNumber());
            preparedStatement.setString(6, customer.email());
            rowsAffected = preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowsAffected;
    }

    @Override
    public int update(Customer customer) {
        int rowsAffected = 0;
        String sql = "UPDATE customer SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? WHERE customer_id = ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, customer.firstName());
            preparedStatement.setString(2, customer.lastName());
            preparedStatement.setString(3, customer.country());
            preparedStatement.setString(4, customer.postalCode());
            preparedStatement.setString(5, customer.phoneNumber());
            preparedStatement.setString(6, customer.email());
            preparedStatement.setInt(7, customer.id());
            rowsAffected = preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowsAffected;
    }

    @Override
    public Customer findByName(String name) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE first_name = ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email"));
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    @Override
    public List<Customer> findWithLimitAndOffset(int limit, int offset) {
        List<Customer> customer = new ArrayList<>();
        String sql = "SELECT * FROM customer LIMIT ? OFFSET ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer.add(new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")));

            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    @Override
    public CustomerCountry findCountryWithMostCustomers() {
        CustomerCountry country = null;
        String sql = "SELECT MAX(country) FROM customer";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                country = new CustomerCountry(resultSet.getString("max"));
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return country;
    }

    @Override
    public CustomerSpender findHighestSpender() {
        CustomerSpender customerSpender = null;
        Customer customer = null;
        String sql =
                """
                SELECT customer.customer_id, customer.first_name, 
                customer.last_name, customer.country, 
                customer.postal_code, customer.phone, 
                customer.email, SUM(total) AS spending FROM customer
                JOIN invoice ON invoice.customer_id = customer.customer_id
                GROUP BY customer.customer_id ORDER BY SUM(total) DESC LIMIT 1
                """;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email"));
                customerSpender = new CustomerSpender(customer, resultSet.getDouble("spending"));
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerSpender;
    }

    @Override
    public List<CustomerGenre> findMostPopularGenre(Customer customer) {
        List<CustomerGenre> genre = new ArrayList<>();
        String sql = """
                SELECT genre.genre_id, genre.name
                FROM customer
                INNER JOIN invoice ON customer.customer_id = invoice.customer_id
                INNER JOIN invoice_line ON invoice.invoice_id = invoice_line.invoice_id
                INNER JOIN track ON track.track_id = invoice_line.track_id
                INNER JOIN genre ON track.genre_id = genre.genre_id
                WHERE customer.customer_id = ?
                GROUP BY genre.genre_id, genre.name ORDER BY COUNT(genre.genre_id) DESC FETCH FIRST 1 ROWS WITH TIES""";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, customer.id());
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                genre.add(new CustomerGenre(
                        resultSet.getInt("genre_id"),
                        resultSet.getString("name")));

            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return genre;
    }
}
