package com.chinook.classjdbcchinook.runners;

import com.chinook.classjdbcchinook.models.CustomerGenre;
import com.chinook.classjdbcchinook.repositories.customer.CustomerRepository;
import com.chinook.classjdbcchinook.view.CustomerPrinter;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CustomerRunner implements ApplicationRunner {
    private final CustomerRepository customerRepository;
    private CustomerPrinter customerPrinter;

    public CustomerRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        customerPrinter = new CustomerPrinter();
        customerPrinter.printCustomer(customerRepository.findAll());
        customerPrinter.printCustomer(customerRepository.findByName("Astrid"));
        customerPrinter.printCustomer(customerRepository.findById(1));
        customerPrinter.printCustomerCountry(customerRepository.findCountryWithMostCustomers());
        customerPrinter.printCustomerSpender(customerRepository.findHighestSpender());

        customerPrinter.printCustomerGenre(customerRepository.findMostPopularGenre(customerRepository.findById(12)));

        customerPrinter.printCustomer(customerRepository.findById(10));
    }
}
