package com.chinook.classjdbcchinook.models;

public record CustomerSpender (Customer customer, double spending) {
}
