package com.chinook.classjdbcchinook.models;

public record CustomerGenre(int id, String name) {
}
