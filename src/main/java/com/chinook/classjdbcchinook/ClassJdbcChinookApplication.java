package com.chinook.classjdbcchinook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassJdbcChinookApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClassJdbcChinookApplication.class, args);
	}

}
