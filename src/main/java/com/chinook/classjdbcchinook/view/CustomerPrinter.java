package com.chinook.classjdbcchinook.view;

import com.chinook.classjdbcchinook.models.Customer;
import com.chinook.classjdbcchinook.models.CustomerCountry;
import com.chinook.classjdbcchinook.models.CustomerGenre;
import com.chinook.classjdbcchinook.models.CustomerSpender;

import java.util.List;

/**
 * A very basic representation of a view, prints out all different types of customers in the console.
 */
public class CustomerPrinter {

    /**
     * Prints out a given customer to the console.
     * @param customer Customer to print to console.
     */
    public void printCustomer(Customer customer) {
        System.out.println("Customer: " + customer.id() + ", "
                + customer.firstName() + ", "
                + customer.lastName() + ", "
                + customer.country() + ", "
                + customer.postalCode() + ", "
                + customer.phoneNumber() + ", "
                + customer.email());
    }

    /**
     * Prints out list of customers to the console.
     * @param customers List of customers to print to console.
     */
    public void printCustomer(List<Customer> customers) {
        for (Customer customer : customers) {
            printCustomer(customer);
        }
    }

    /**
     * Prints out given customer country to the console.
     * @param customerCountry Customer countries to print to console.
     */
    public void printCustomerCountry(CustomerCountry customerCountry) {
        System.out.println("Country: " + customerCountry.name());
    }

    /**
     * Prints out list of customer countries to the console.
     * @param customerCountries List of customer countries to print to console.
     */
    public void printCustomerCountry(List<CustomerCountry> customerCountries) {
        for (CustomerCountry country : customerCountries) {
            printCustomerCountry(country);
        }
    }

    /**
     * Prints out a genre to the console.
     * @param genre List of genres to print to console.
     */
    public void printCustomerCountry(CustomerGenre genre) {
        System.out.println(genre.id() + ", " + genre.name());
    }

    /**
     * Prints out a list of customer genres to the console.
     * @param genres List of customer genres to print to console.
     */
    public void printCustomerGenre(List<CustomerGenre> genres) {
        for (CustomerGenre genre : genres) {
            printCustomerCountry(genre);
        }
    }

    /**
     * Prints out a customer spender to the console.
     * @param customerSpender Customer spender to print to console.
     */
    public void printCustomerSpender(CustomerSpender customerSpender) {
        System.out.print("Customer spender: ");
        printCustomer(customerSpender.customer());
        System.out.println(customerSpender.spending());
    }
}
