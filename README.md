# Assignment - Data access with JDBC

In this project we created a database with SQL script. With this scirpts we setup some tables, added relationships between the tables and then inserted data. We also build a Spring Boot Application in Java that interacted with the database Chinook. Chinook is a database that contains customers who purchasing songs. 

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Contributors](#contributors)

## Background

This is a Spring Boot application that is the result of the Assignment in _Module 6_ of the Java Full Stack course. The application mainly focuses on SQL and Spring Boot. The tools we used was IntelliJ, PgAdmin and Postgres SQL driver dependency. This project is created with a __Gradle__ build system.

All the SQL Scripts for our created database SuperheroesDB is in the folder `SQL Scripts (Appendix A)`. This scripts includes:

- Creating tables
- Relationships between tables
- Inserting data
- Updating data
- Deleting data

In our Spring Boot application you can access data from the Chinook database through the following methods: 

- `findAll()`
- `findById()`
- `findByName()`
- `findWithLimitAndOffset()`
- `insert()`
- `update()`
- `findCountryWithMostCustomers()`
- `findHighestSpender()`
- `findMostPopularGenre()`

## Install

- Install JDK 17
- Install Intellij
- Install PgAdmin
- Clone repository:
```
https://gitlab.com/OliverDahlqvist/Java-Assignment-2-Database.git
```

## Usage

1. Add database Chinook into PgAdmin.  
2. Change the `application.properties` file to the following: 
```
spring.datasource.url=jdbc:postgresql://localhost:5432/<database name>
spring.datasource.username=<your username>
spring.datasource.password=<your password>
```
3. Run the application

## Contributors

[@johannaolsson](https://gitlab.com/johannaolsson)
[@OliverDahlqvist](https://gitlab.com/OliverDahlqvist)
